document.addEventListener('DOMContentLoaded', () => {
    fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(response => response.json())
        .then(films => {
            films.sort((a, b) => a.episodeId - b.episodeId); 
            displayFilms(films);
            films.forEach(film => {
                fetchFilmCharacters(film);
            });
        })
        .catch(error => console.error('Error fetching films:', error));
});

function displayFilms(films) {
    const filmsContainer = document.getElementById('films-container');
    films.forEach(film => {
        const filmElement = document.createElement('div');
        filmElement.classList.add('film');
        filmElement.id = `film-${film.id}`;
        
        filmElement.innerHTML = `
            <h2>Episode ${film.episodeId}: ${film.name}</h2>
            <p>${film.openingCrawl}</p>
            <div class="loading" id="loading-${film.id}"></div>
            <ul class="characters" id="characters-${film.id}"></ul>
        `;
        
        filmsContainer.appendChild(filmElement);
    });
}

function fetchFilmCharacters(film) {
    const characterPromises = film.characters.map(url => fetch(url).then(response => response.json()));
    
    Promise.all(characterPromises)
        .then(characters => {
            displayCharacters(film.id, characters);
        })
        .catch(error => console.error(`Error fetching characters for film ${film.id}:`, error));
}

function displayCharacters(filmId, characters) {
    const charactersContainer = document.getElementById(`characters-${filmId}`);
    const loadingElement = document.getElementById(`loading-${filmId}`);
    loadingElement.style.display = 'none'; 
    
    charactersContainer.innerHTML = ''; 
    
    characters.forEach(character => {
        const characterElement = document.createElement('li');
        characterElement.textContent = character.name;
        charactersContainer.appendChild(characterElement);
    });
}